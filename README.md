# Roman

![Hackerman](https://pbs.twimg.com/profile_images/1035079978008948737/NNtdoxpw_400x400.jpg)

## Description
__FR__ - Ce projet permet de convertir des nombres arabes en un nombre Romain.

__EN__ - This project aims to transform a arabic number into roman one.

## Statut
WIP -> En Cours de developpement.  
Allez sur le site http://lesite.com.

## Documentation
(c.f. : README.md)

## Exemple

````bash
3 -> III
4 -> IV
````
```mermaid
graph LR
A[Début] -- chiffre = 3  --> B{chiffre < 4 }
B -- chiffreRomain --> C[for =0 i < chffre i++]
C -- chiffreRomain += I  --> C
C -- chiffreRomaine = III --> D[Fin]
```

## Structure

-   `src/app`
-   `config`
-   `bin`

## Install

1. Clone and initialize the repository.

Clone with HTTPS: [https://gitlab.com/JalilArf/roman.git](https://gitlab.com/JalilArf/roman.git)

```shell
$ git clone https://gitlab.com/JalilArf/roman.git
$ git init
```

2. Install dependencies.

First, install Node.js.  
Then, write these commands :
```shell
$ cd roman
$ npm init
$ npm install
```

3. Install vscode extensions.

Install the recommended extensions via the extension tab.

4. Build and run the project.

Double-click on index.js and write down in the terminal :

```shell
$ npm run build
```

## Auteurs

### Creator 

Jalil Arfaoui

### Contribution

* Guillaume dax
* Killian RENOLEAU `\[T]/`
* Claudian CAMUS `|[O]_`
* Aleksandrvassilyev95
* Steven Ladowichx
* Pierre Tubeuf
* Nicolas Glories
* Retro Gaming Test
* Elise
* Lison
* Nona tux
* Clément Molinié
* Stephen Soupart
* Loic
* Théo VIALA
* Quentin

This project is developped by the students of ESN81.
