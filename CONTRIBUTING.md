# contribution

Do you like help ?

    You can join the repository https://gitlab.com/JalilArf/roman.git and help
    the team to contribute on this project,
    We works with merge request and Issues


You can contribute in the following ways:

    Finding and reporting bugs
    Contributing code to Roman by fixing bugs or implementing features
    Improving the documentation 


# Merge_Request

Template for merge requests :
   • Description of the change
   • Applicable issue numbers
   • Alternate designs/implementations
   • Benefits of this implementation
   • Possible drawbacks
   • Why should this be part of a core component?
   • Testing process


# Documentation

     README.md






